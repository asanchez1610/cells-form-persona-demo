/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
  --margin-botom-field: 5px;
  --bg-color-invalid: #F4C3CA;
  --color-invalid: #B92A45;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.form-container {
  display: flex;
  align-items: center;
  flex-direction: column;
}
.form-container bbva-form-field {
  margin-bottom: var(--margin-botom-field);
  width: 100%;
}
.form-container .invalid {
  --_field-bg-color: var(--bg-color-invalid);
  --_field-input-color: var(--color-invalid);
  --_field-label-color: var(--color-invalid);
  --_field-border-color: var(--color-invalid);
  --_field-focused-border-color: var(--color-invalid);
  --_field-focused-label-color: var(--color-invalid);
}
.form-container .content-buttons {
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
}
`;
