import { html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement.js';
import styles from './CellsFormPersonaDemo-styles.js';
import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-button-default';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-form-persona-demo></cells-form-persona-demo>
```

##styling-doc

@customElement cells-form-persona-demo
*/
export class CellsFormPersonaDemo extends BaseElement {
  static get is() {
    return 'cells-form-persona-demo';
  }

  // Declare properties
  static get properties() {
    return {
      person: Object,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.person = {};
    this.init();
  }

  async init() {
    await this.updateComplete;
    const inputs = this.elementsAll('bbva-form-field');
    inputs.forEach((element) => {
      element.addEventListener('input', evt => this.setPerson(evt, element));
    });
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-form-persona-demo-shared-styles'),
    ];
  }

  setPerson({ target }, formField) {
    const tmp = { ...this.person };
    tmp[target.name] = target.value;
    this.person = tmp;
    if (this.isEmpty(target.value)) {
      formField.classList.add('invalid');
    } else {
      formField.classList.remove('invalid');
    }
  }

  validateForm() {
    const inputs = this.elementsAll('.input-form');
    let errors = 0;
    inputs.forEach((input) => {
      if (this.isEmpty(input.value)) {
        input.classList.add('invalid');
        errors += 1;
      } else {
        input.classList.remove('invalid');
      }
    });
    return errors === 0;
  }

  registrar() {
    if (this.validateForm()) {
      this.dispatch('on-register-person', this.person);
      this.reset();
    }
  }

  reset() {
    this.person = {};
    const requerids = this.elementsAll('.input-form');
    requerids.forEach((element) => {
      element.classList.remove('invalid');
    });
    this.dispatch('on-cancel-form', true);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <div class="form-container">
        <bbva-form-field
          .value="${this.person.nombres ? this.person.nombres : ''}"
          name="nombres"
          class="input-form"
          label="Nombres"
          optional-label=""
        ></bbva-form-field>

        <bbva-form-field
          .value="${this.person.email ? this.person.email : ''}"
          name="email"
          class="input-form"
          label="Email"
          optional-label=""
        ></bbva-form-field>

        <bbva-form-field
          .value="${this.person.telefono ? this.person.telefono : ''}"
          name="telefono"
          class="input-form"
          label="Telefono"
          optional-label=""
        ></bbva-form-field>

        <div class="content-buttons">
          <bbva-button-default class="secondary" @click="${this.reset}">Cancelar</bbva-button-default>
          <bbva-button-default @click="${this.registrar}">${this.person._id ? 'Editar' : 'Registrar'}</bbva-button-default>
        </div>
      </div>
    `;
  }
}
