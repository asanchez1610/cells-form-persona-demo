import {
  html, fixture, assert, fixtureCleanup,
} from '@open-wc/testing';
import '../cells-form-persona-demo.js';

suite('CellsFormPersonaDemo', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-form-persona-demo></cells-form-persona-demo>`);
    await el.updateComplete;
  });

  test('instantiating component', () => {
    const element = el.shadowRoot.querySelector('.form-container');
    assert.isAccessible(element);
  });

  test('Test function setPerson', () => {
    const elment = document.createElement('div');
    const formField = document.createElement('input');
    const evt = {
      target: elment,
    };
    el.setPerson(evt, formField);
  });

  test('Test function setPerson with value formField', () => {
    const elment = document.createElement('input');
    const formField = document.createElement('input');
    elment.value = 'demo';
    const evt = {
      target: elment,
    };
    el.setPerson(evt, formField);
  });

  test('Test function validateForm', () => {
    el.validateForm();
  });

  test('Test function registrar', () => {
    const inputs = el.shadowRoot.querySelectorAll('.input-form');
    inputs.forEach((input) => {
      const elementInput = input;
      elementInput.value = 'demo';
    });
    el.registrar();
  });

  test('Test function registrar Invalid', () => {
    el.registrar();
  });

  test('Test person with value', () => {
    const person = {
      nombres: 'Arturo',
      email: 'demo@gmail.com',
      telefono: '3216545645',
    };
    el.person = person;
  });

  test('Test events input', () => {
    const inputs = el.shadowRoot.querySelectorAll('.input-form');
    const inputEvent = new CustomEvent('input', {
      detail: true,
      bubbles: true,
      composed: true,
    });
    inputs.forEach((input) => {
      input.dispatchEvent(inputEvent);
    });
  });
});
